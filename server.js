'use strict'

const express = require('express') //import มาแต่ยังไม่ได้เอาไปใช้
const app = express() //ต้องสร่้าง instance ของ express
const port = 8000

//app.get() รับ param สองตัวเป็นอย่างน้อย ตัวแรกเป็น path ตัวที่สองเป็น function(request, response)
//เป็นคำสั่งสร้าง path ว่าเป็น method get ถ้า client เรียก path นี้ ให้รันฟังก์ชั่นใน app.get()
app.get('/movies', (req, res) => {
    const results = [{
        title: 'fake title 1',
        image_url: 'fake image url',
        overview: 'fake overview'

    },{
        title: 'fake title 2',
        image_url: 'fake image url',
        overview: 'fake overview'
    }]
    
    res.json(results)
}) 

//โปรแกรมจะเวิร์คได้ต้องเอาไปผูกกับ port บางอย่างเพื่อที่จะรอรับ request network
//app.listen() ให้สร้าง port ที่ port number นี้ๆๆๆๆ
app.listen(port, () => {console.log(`started at ${port}`)})